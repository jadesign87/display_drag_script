function endMove() {
    $(this).removeClass('movable');
}

function startMove() {
    $('.movable').on('mousemove', function(event) {
        var thisX = event.pageX - $(this).width() / 2,
            thisY = event.pageY - $(this).height() / 2;
			
		$('.movable').offset({
            left: thisX,
            top: thisY
        });
        
        x = thisX;
        y = thisY;

    });
}

$(document).ready(function() {

    $('body').append('<p><textarea class="js-copytextarea"></textarea></p>');
    $('body').append('<p><button class="js-textareacopybtn">Kopiuj wartość</button></p>');
    $('body').append('<p><button id="run-script">START SCRIPT</button></p>');




    $('#run-script').click(function(){

        $('a').removeAttr('href');
        $('.container div').css('transform', 'none');
        $('.container div').removeClass('active');
        $('.container div').css('transition', 'none')

        
        $(".container div").on('click', function() {
        	event.stopPropagation();
            $(this).addClass('movable');
            startMove();
        });
        $('body').on('dblclick keyup', function() {
        // .on('dblclick', function() {
            $('.movable').removeClass('movable');
            
    		x = (Math.round(x, 2));
    		y = (Math.round(y, 2));

            $('.js-copytextarea').val( x+'px, '+ y+'px'  );
            
            endMove();
            $('button').click();
        });


    	var copyTextareaBtn = document.querySelector('.js-textareacopybtn');

    	copyTextareaBtn.addEventListener('click', function(event) {
    	  var copyTextarea = document.querySelector('.js-copytextarea');
    	  copyTextarea.select();

    	  try {
    	    var successful = document.execCommand('copy');
    	    var msg = successful ? 'successful' : 'unsuccessful';
    	    console.log('Copying text command was ' + msg);
    	  } catch (err) {
    	    console.log('Oops, unable to copy');
    	  }
    	});

    });//end runScript if



});